/*    */ package somepeople.memspawn.events;
/*    */ 
/*    */ import org.bukkit.entity.Entity;
/*    */ import org.bukkit.entity.Player;
/*    */ import org.bukkit.event.EventHandler;
/*    */ import org.bukkit.event.EventPriority;
/*    */ import org.bukkit.event.Listener;
/*    */ import org.bukkit.event.entity.EntityDamageEvent;
/*    */ import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
/*    */ import org.bukkit.event.player.PlayerJoinEvent;
/*    */ import somepeople.memspawn.main.Main;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Events
/*    */   implements Listener
/*    */ {
/*    */   @EventHandler(priority=EventPriority.LOWEST)
/*    */   public void onPlayerFalling(EntityDamageEvent event)
/*    */   {
/* 26 */     Entity ent = event.getEntity();
/*    */     
/* 28 */     if (((ent instanceof Player)) && (event.getCause() == EntityDamageEvent.DamageCause.VOID))
/*    */     {
/* 30 */       Player player = (Player)ent;
/*    */       
/* 32 */       player.teleport(Main.spawn);
/*    */     }
/*    */   }
/*    */   
/*    */   @EventHandler
/*    */   public void onPlayerJoin(PlayerJoinEvent event)
/*    */   {
/* 39 */     if (Main.spawn != null) event.getPlayer().teleport(Main.spawn);
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemSpawn-1.0.jar!\somepeople\memspawn\events\Events.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */
/*    */ package somepeople.memspawn.commands;
/*    */ 
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.command.Command;
/*    */ import org.bukkit.command.CommandExecutor;
/*    */ import org.bukkit.command.CommandSender;
/*    */ import org.bukkit.entity.Player;
/*    */ import somepeople.memspawn.main.Main;
/*    */ import somepeople.memspawn.main.Message;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SpawnCommand
/*    */   implements CommandExecutor
/*    */ {
/*    */   public boolean onCommand(CommandSender sender, Command command, String alias, String[] args)
/*    */   {
/* 29 */     if (alias.equalsIgnoreCase("spawn"))
/*    */     {
/* 31 */       switch (args.length) {
/*    */       case 0: 
/*    */         try {
/* 34 */           Player player = (Player)sender;
/* 35 */           player.teleport(Main.spawn);
/* 36 */           sender.sendMessage(Message.getMessage("messages.spawn"));
/*    */         } catch (NullPointerException e) {
/* 38 */           sender.sendMessage(Message.getMessage("messages.nospawn"));
/*    */         }
/*    */       
/*    */       case 1: 
/* 42 */         if (args[0].equalsIgnoreCase("help")) {
/* 43 */           sender.sendMessage(Message.getMessage("messages.help"));
/* 44 */         } else if (args[0].equalsIgnoreCase("all")) {
/* 45 */           if (sender.hasPermission("memspawn.spawnall"))
/*    */           {
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 61 */             for (Player p : Bukkit.getOnlinePlayers()) {
/* 62 */               p.teleport(Main.spawn);
/* 63 */               p.sendMessage(Message.getMessage("messages.spawn"));
/*    */             }
/* 65 */             sender.sendMessage(Message.getMessage("messages.spawnall"));
/*    */           }
/*    */           else {
/* 68 */             sender.sendMessage(Message.getMessage("messages.nopex"));
/*    */           }
/*    */         }
/* 71 */         else if (sender.hasPermission("memspawn.spawnplayer")) {
/*    */           try {
/* 73 */             Player target = Bukkit.getPlayer(args[0]);
/* 74 */             target.teleport(Main.spawn);
/* 75 */             target.sendMessage(Message.getMessage("messages.spawn"));
/*    */             
/* 77 */             sender.sendMessage(Message.getMessage("messages.spawnplayer"));
/*    */           } catch (NullPointerException e) {
/* 79 */             sender.sendMessage(Message.getMessage("messages.noplayer"));
/*    */           }
/*    */         } else
/* 82 */           sender.sendMessage(Message.getMessage("messages.nopex"));
/*    */         break;
/*    */       }
/*    */     }
/* 86 */     return false;
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemSpawn-1.0.jar!\somepeople\memspawn\commands\SpawnCommand.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */
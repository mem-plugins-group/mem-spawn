/*    */ package somepeople.memspawn.commands;
/*    */ 
/*    */ import org.bukkit.command.Command;
/*    */ import org.bukkit.command.CommandExecutor;
/*    */ import org.bukkit.command.CommandSender;
/*    */ import org.bukkit.entity.Player;
/*    */ import somepeople.memspawn.main.Main;
/*    */ import somepeople.memspawn.main.Message;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SetSpawnCommand
/*    */   implements CommandExecutor
/*    */ {
/*    */   public boolean onCommand(CommandSender sender, Command command, String alias, String[] args)
/*    */   {
/* 29 */     if (alias.equalsIgnoreCase("setspawn")) {
/* 30 */       if (sender.hasPermission("memspawn.setspawn")) {
/* 31 */         Main.setSpawn(((Player)sender).getLocation());
/*    */         
/* 33 */         sender.sendMessage(Message.getMessage("messages.setspawn"));
/*    */       } else {
/* 35 */         sender.sendMessage(Message.getMessage("messages.nopex"));
/*    */       }
/*    */     }
/* 38 */     return false;
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemSpawn-1.0.jar!\somepeople\memspawn\commands\SetSpawnCommand.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */
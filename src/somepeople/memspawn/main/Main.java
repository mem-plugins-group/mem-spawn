/*    */ package somepeople.memspawn.main;
/*    */ 
/*    */ import java.io.File;
/*    */ import java.io.IOException;
/*    */ import java.io.PrintStream;
/*    */ import java.util.logging.Logger;
/*    */ import net.md_5.bungee.api.ChatColor;
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.Location;
/*    */ import org.bukkit.Server;
/*    */ import org.bukkit.command.PluginCommand;
/*    */ import org.bukkit.configuration.file.FileConfiguration;
/*    */ import org.bukkit.configuration.file.FileConfigurationOptions;
/*    */ import org.bukkit.plugin.PluginManager;
/*    */ import org.bukkit.plugin.java.JavaPlugin;
/*    */ import somepeople.memspawn.commands.MemSpawnCommand;
/*    */ import somepeople.memspawn.commands.SpawnCommand;
/*    */ import somepeople.memspawn.events.Events;
/*    */ 
/*    */ public class Main extends JavaPlugin
/*    */ {
/*    */   public static FileConfiguration config;
/*    */   public static double rotate;
/*    */   public static Location spawn;
/*    */   
/*    */   public void onEnable()
/*    */   {
/* 28 */     config = getConfig();
/*    */     
/* 30 */     setConfig();
/*    */     
/* 32 */     config.options().copyDefaults(true);
/* 33 */     saveConfig();
/*    */     try
/*    */     {
/* 36 */       spawn = new Location(Bukkit.getServer().getWorld(config.getString("spawn.world")), 
/* 37 */         config.getDouble("spawn.x"), config.getDouble("spawn.y"), config.getDouble("spawn.z"));
/* 38 */       spawn.setYaw((float)config.getDouble("spawn.rotate"));
/*    */     } catch (NullPointerException e) {
/* 40 */       getLogger().info(ChatColor.RED + "No world in config file. Enter world name and restart plugin");
/*    */     }
/*    */     
/* 43 */     getCommand("spawn").setExecutor(new SpawnCommand());
/* 44 */     getCommand("setspawn").setExecutor(new somepeople.memspawn.commands.SetSpawnCommand());
/* 45 */     getCommand("memspawn").setExecutor(new MemSpawnCommand());
/* 46 */     getServer().getPluginManager().registerEvents(new Events(), this);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void onDisable() {}
/*    */   
/*    */ 
/*    */   static void setConfig()
/*    */   {
/* 56 */     config.addDefault("spawn.x", Integer.valueOf(0));
/* 57 */     config.addDefault("spawn.y", Integer.valueOf(0));
/* 58 */     config.addDefault("spawn.z", Integer.valueOf(0));
/* 59 */     config.addDefault("spawn.rotate", Double.valueOf(0.0D));
/* 60 */     config.addDefault("spawn.world", "");
/*    */     
/* 62 */     config.addDefault("messages.spawn", "[MemSpawn] Вы телепортированы на spawn");
/* 63 */     config.addDefault("messages.setspawn", "[MemSpawn] Локация spawn установлена");
/* 64 */     config.addDefault("messages.spawnall", "[MemSpawn] Все игроки телепортированы");
/* 65 */     config.addDefault("messages.spawnplayer", "[MemSpawn] Игрок телепортирован на spawn");
/*    */     
/* 67 */     config.addDefault("messages.nospawn", "[MemSpawn] Локация spawn не установлена");
/* 68 */     config.addDefault("messages.noplayer", "[MemSpawn] Этот игрок не в сети");
/*    */     
/* 70 */     config.addDefault("messages.nopex", "[MemSpawn] У Вас нет на это прав!");
/*    */     
/* 72 */     config.addDefault("messages.help", 
/* 73 */       "/spawn - телепортировать себя к spawn\n/spawn <игрок> - телепортировать игрока к spawn\n/spawn all - телепортировать всех игроков к spawn\n/setspawn - установить локацию spawn (текущие координаты игрока)");
/*    */   }
/*    */   
/*    */ 
/*    */   public static void setSpawn(Location newspawn)
/*    */   {
/* 79 */     rotate = newspawn.getYaw();
/*    */     
/* 81 */     spawn = newspawn;
/*    */     
/* 83 */     config.set("spawn.x", Integer.valueOf(spawn.getBlockX()));
/* 84 */     config.set("spawn.y", Integer.valueOf(spawn.getBlockY()));
/* 85 */     config.set("spawn.z", Integer.valueOf(spawn.getBlockZ()));
/* 86 */     config.set("spawn.rotate", Float.valueOf(spawn.getYaw()));
/* 87 */     config.set("spawn.world", spawn.getWorld().getName());
/*    */     try
/*    */     {
/* 90 */       config.save(new File("plugins/MemSpawn/config.yml"));
/*    */     } catch (IOException e) {
/* 92 */       System.out.println("Error save config file");
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\MemSpawn-1.0.jar!\somepeople\memspawn\main\Main.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */